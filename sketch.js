/** Change the variables below to analyse new videos */

let file = 'Main_Street.mp4';
let file_width = 1920;
let file_height = 1080;

/** Change the above below to analyse new videos */



// -------------------------------------------------------------------------------------



// Video controls
let video;
let yolo;
let playing;

// Action controls
let playPauseButton;
let timeDisplay;

// Video recording
let stream;
let recorder;
let chunks = [];

// Object detection
let objects = [];
let table;

function setup() {
    // console.log('ml5 version:', ml5.version);

    playing = false;

    // Load the video
    video = createVideo(['data/' + file]);

    // Create a visible canvas
    createCanvas(1000, file_height / file_width * 1000 + 30);
    video.hide();

    // Set up YOLO
    yolo = ml5.YOLO(video);

    // Create action buttons
    playPauseButton = createButton('Start detection');
    playPauseButton.position(10, 10);
    playPauseButton.mousePressed(playVideoStartRecording);

    timeDisplay = createInput();
    timeDisplay.position(200, 10);

    // Create video recording
    stream = document.querySelector('canvas').captureStream(30);
    recorder = new MediaRecorder(stream, {
        videoBitsPerSecond: 2500000,
        mimeType: 'video/webm'
    });

    // Create data recording
    table = new p5.Table();
    table.addColumn('time');
    table.addColumn('obj_count');
    table.addColumn('obj_number');
    table.addColumn('obj_name');
    table.addColumn('obj_confidence');
    table.addColumn('obj_x');
    table.addColumn('obj_y');
    table.addColumn('obj_h');
    table.addColumn('obj_w');
}

function playVideoStartRecording() {
    if(!playing){
        video.play();
        detect();
        record();

        playing = true;

        playPauseButton.html('Please wait...');
    }
}

function stopVideoAndRecording(){
    playing = false;
    recorder.stop();
}


// Make sure we can view what is going on
// This function is called as fast as computer can compute
function draw() {
    image(video, 0, 30, width, height - 30);
    timeDisplay.value(video.time() + " / " + video.duration());

    if(video.time() == video.duration()){
        stopVideoAndRecording();
    }

    for (let i = 0; i < objects.length; i++) {
        noStroke();
        fill(0, 255, 0);
        text(objects[i].label + " (" + (Math.round(objects[i].confidence * 10000) / 100) + "%)", objects[i].x * width, objects[i].y * height - 5);
        noFill();
        strokeWeight(4);
        stroke(0, 255, 0);
        rect(objects[i].x * width, objects[i].y * height, objects[i].w * width, objects[i].h * height);
    }
}

// Detect the images on the current frame
function detect() {
    yolo.detect(function (err, results) {
        objects = results;

        console.log(objects);

        objects.forEach((object, k) => {
            let newRow = table.addRow();

            newRow.setNum('time', video.time());
            newRow.setNum('obj_count', objects.length);

            newRow.setNum('obj_number', k + 1); // count like normal people
            newRow.setString('obj_name', object.label);
            newRow.setNum('obj_confidence', object.confidence);
            newRow.setNum('obj_x', object.x);
            newRow.setNum('obj_y', object.y);
            newRow.setNum('obj_h', object.h);
            newRow.setNum('obj_w', object.w);
        });

        if (playing) {
            // Do it again if we are playing it
            detect();
        }
    });
}

// Record all data to a video
function record() {
    chunks.length = 0;

    recorder.ondataavailable = e => {
        if (e.data.size) {
            chunks.push(e.data);
        }
    };

    recorder.onstop = exportData;
    recorder.start();
}


// Export video and csv
function exportData(e) {
    var blob = new Blob(chunks);
    doSave(blob, 'video/webm', 'output.webm');
    saveTable(table, 'output.csv');
    playPauseButton.html('Start detection');
}

// Helper function to save webm files
function doSave(value, type, name) {
    var blob = value;
    var URL = window.URL || window.webkitURL;
    var bloburl = URL.createObjectURL(blob);
    var anchor = document.createElement("a");
    if ('download' in anchor) {
        anchor.style.visibility = "hidden";
        anchor.href = bloburl;
        anchor.download = name;
        document.body.appendChild(anchor);
        var evt = document.createEvent("MouseEvents");
        evt.initEvent("click", true, true);
        anchor.dispatchEvent(evt);
        document.body.removeChild(anchor);
    } else if (navigator.msSaveBlob) {
        navigator.msSaveBlob(blob, name);
    } else {
        location.href = bloburl;
    }
}